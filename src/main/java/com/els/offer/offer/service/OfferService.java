package com.els.offer.offer.service;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.els.offer.offer.model.Offer;

@Service
public class OfferService {

    List<Offer> offersList  = new ArrayList<Offer> ();
	
	public Offer save(Offer offer) {
		
		if (offer != null && offer.getTitle() != null ) {
			offer.setId(offersList.size() + 1);
			offersList.add( offer);
			return offer;
		}
		return null;
	}

	public Offer update(Offer offer) {
				
		if (offer != null && offer.getTitle() != null && offer.getId() >= 1) {
			  Offer existingOffer = offersList.get(offer.getId());
			  if (existingOffer != null) {
				  offersList.set(offer.getId() - 1, offer);
			  }
			
			return offer;
		}
		
		return null;
	}

	public Offer findOffer(Integer id) {
		if (offersList != null && offersList.size() > 0 ) {
			Offer offer = offersList.get(id - 1);
			return offer;
		}
		return null;
	}
	

}
